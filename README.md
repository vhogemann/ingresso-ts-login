# Lab Day - 19/11/2015

_Lab Days_ são um tempo que nós, da equipe do novo Checkout da Ingresso.com, reservamos para estudar soluções e novas formas de melhorar nosso produto,
e a forma como conduzimos nossos projetos. Esse POC é o resultado de um desses estudos.

### Como usar esse projeto

Estamos usando um template de projeto do [generator-gulp-angular](https://github.com/Swiip/generator-gulp-angular), então basta ir a página do projeto
e seguir as instruções. Mas, resumidamente você precisa ter instalado o [NodeJS](https://nodejs.org/), e o [NPM](https://www.npmjs.com/).

Depois, basta instalar os seguintes pacotes:

```
$ npm install -g gulp bower typescript tsd
```

Com isso instalado, entre no diretório do projeto e instale as dependências com _npm_ e o _bower_

``` 
$ cd ingresso-ts-login
$ npm install
$ bower install
```

Para rodar o projeto, e abrir um browser faça:

```
$ gulp serve
```

## Objetivo

Queriamos ver se migrar para TypeScript, abandonando o JavaScript, é uma solução viável para o desenvolvimento das nossas aplicaçãoes de frontend.
E testar um workflow pronto de trabalho, com compilação, minificação e empacotamento dos assets de uma aplicação.

## O quê

Abaixo tem a relação de coisas que nós conseguimos testar no LabDay, e uma breve descrição de cada uma delas.

* __TypeScript__

	Linguagem de programação, que compila para JavaScript, mas que oferece coisas como Tipagem Estática, Classes e Interfaces, Generics e um sistema de módulos.
	Queríamos ver se ela nos ajudaria a impor ordem a bangunça que o JavaScript normalmente é, e o quanto seria fácil ou difícil se adaptar a ela.

* __AngularJS__

	O interesse aqui é ver como usar AngularJS com TypeScript. O fato do AngularJS 2.0 estar sendo desenvolvido em TypeScript é um bom sinal,
	mas queremos havaliar se mudar de linguagem iria facilitar ou dificultar as coisas.
* __Gulp__

	Fato que nós não somos muito felizes com o Grunt. Vamos ver se um projeto com Gulp facilita nossa vida.
	
	* __Minify__
	
		Minificar eficientemente o código é um dos requisitos que o Gulp precisa atender.
	
	* __SourceMaps__
	
		Gerar SourceMaps é fundamental pra gente conseguir debugar o código TypeScript no browser, sem isso o desenvolvimento fica praticamente as escuras.
		Além disso, hoje em produção quando acontece um erro, ficamos as escuras sem saber direito onde foi o problema, por conta do código minificado sem
		sourcemaps.
	
	* __Yeoman__
		
		O Yeoman tem uma coleção bem grande de templates de projetos, vamos pegar um pronto pra ver se ajuda.
		
		* __generator-gulp-angular__
		
			Esse é o template que escolhemos, ele tem uma série de opções pra gerar o projeto... entre elas usar TypeScript, e integrar o Bootstrap3.

## Como

Montamos uma pequena aplicação, que efetua login na nossa __API__, e mantém o usuário logado utilizando __cookies__. A aplicação está disponível no _BitBucket_:

	[https://bitbucket.org/vhogemann/ingresso-ts-login]

## Bom

### Typescript

Realmente nos ajudou a organizar melhor o código, e o fato de existirem definições de tipos para diversos frameworks JS, incluindo o Angular, ajuda a evitar
muitos erros comuns. Muita coisa é pega na hora de compilar o código, ou pela IDE.

O VS Code também ajuda muito, com suporte a auto-complete do código, e mostrando erros na hora.

Um efeito colateral do código fortemente tipado é que quando você define bem as interfaces, o código fica auto-documentado. E com suporte da IDE e auto-complete,
fica muito mais fácil programar.

Com uso de _sourcemaps_, debugar o código no browser é tão fácil quanto se estivéssemos utilizando JavaScript. E o Chrome ainda suporta destaque de sintaxe dos
fontes TS!

### Yeoman e generator-gulp-angular

Esse generator é absurdamente completo. E a estrutura da aplicação gerada é bem lógica, e fácil de entender. Poupou muito tempo e trabalho.

## Ruim

### Complexidade do Build

Nem tudo são flores, e os arquivos de configuração do build utilizando Gulp são tão arcanos quanto os do Grunt numa primeira olhada... Mas parecem melhor organizados.
Vamos precisar perder um tempo pra entender o que está acontecendo, e poder ter confiança de mudar alguma coisa se for preciso.

## Conclusão

Acredito que TypeScript seja o caminho, não tivemos grandes dificuldades, e conseguimos bons ganhos em termos de organização e diminuição de erros no código.
Ainda precisamos testar em um projeto _de verdade_, e ver como atingir maior modularidade e compartilhar código entre projetos diferentes.