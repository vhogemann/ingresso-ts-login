/// <reference path="../../.tmp/typings/tsd.d.ts" />

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { Authentication } from './components/authentication/authentication.service';
import { LoginManager } from './components/login-manager/login-manager.service';
import { LoginController } from './login/login.controller';
import { MainController } from './main/main.controller';

declare var moment: moment.MomentStatic;

module ingressoTsLogin {
  'use strict';

  angular.module('ingressoTsLogin', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngRoute', 'ui.bootstrap', 'toastr'])
    .constant('moment', moment)
    .constant('API_PATH', 'http://api.ingresso.ideais.local')
    .constant('COOKIE_DOMAIN', '')
    .config(config)
    .config(routerConfig)
    .run(runBlock)
    .service('loginManager', LoginManager)
    .service('auth', Authentication)
    .controller('LoginController', LoginController)
    .controller('MainController', MainController);
}
