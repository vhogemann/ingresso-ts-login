export class Authentication {

	private $q: ng.IQService;
	private $http: ng.IHttpService;
	private API_PATH: string;
	
	/** @ngInject */
	constructor($q: ng.IQService, $http: ng.IHttpService, API_PATH: string) {
		this.$http = $http;
		this.$q = $q;
		this.API_PATH = API_PATH;
	}

	doAuthenticate(credential: Credential): ng.IPromise<Credential> {
		var defered = this.$q.defer<Credential>();
		this.$http({
			method: 'POST',
			url: `${this.API_PATH}/token`,
			withCredentials: true,
			data: $.param({
				username: credential.username,
				password: credential.password,
				grant_type: 'password'
			}),
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		})
			.success((data: any) => {
				credential.token = data.access_token;
				credential.data = {
					firstName: data.user_first_name,
					id: data.user_id
				}
				defered.resolve(credential);
			})
			.error(defered.reject);
		return defered.promise;
	}
}

export class Credential {
	public username: string;
	public password: string;
	public token: string;
	public data: any;
}