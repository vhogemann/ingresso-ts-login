import { Authentication, Credential } from "../authentication/authentication.service";

export class LoginManager {

	private auth: Authentication;
	private $q: ng.IQService;
	private $cookies: ng.cookies.ICookiesService;

	private COOKIE_DOMAIN: string;

	private credential: ng.IPromise<Credential>;
	
	
	/** @ngInject */
	constructor(auth: Authentication, $q: ng.IQService, $cookies: ng.cookies.ICookiesService, COOKIE_DOMAIN: string) {
		this.auth = auth;
		this.$q = $q;
		this.$cookies = $cookies;
		this.credential = null;
		this.COOKIE_DOMAIN = COOKIE_DOMAIN;
	}

	doLogin(credential: Credential): ng.IPromise<Credential> {
		var defered = this.$q.defer<Credential>();
		this.credential = defered.promise;

		this.$cookies.remove('auth_cookie');

		this.auth.doAuthenticate(credential)
			.then((credential: Credential) => {
				var expiration = new Date();
				var time = expiration.getTime() + (24 * 60 * 60 * 1000);
				expiration.setTime(time);

				this.$cookies.putObject('auth_cookie', credential, { path: '/', domain: this.COOKIE_DOMAIN, expires: expiration });
				defered.resolve(credential);
			})
			.catch(defered.reject);

		return this.credential;
	}

	doLogout() {
		this.credential = null;
		this.$cookies.remove('auth_cookie');
	}

	getPrincipal(): ng.IPromise<Credential> {

		if (this.credential != null)
			return this.credential;

		var defered = this.$q.defer<Credential>();

		var cookie = null;

		try {
			cookie = <Credential>this.$cookies.getObject('auth_cookie');
		} catch (ex) {
			//TODO
		}

		if (cookie != null) {
			defered.resolve(cookie);
		} else {
			this.$cookies.remove('auth_cookie');
			defered.reject();
		}

		return defered.promise;
	}
}