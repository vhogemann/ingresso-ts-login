import {LoginManager} from '../components/login-manager/login-manager.service';
import {Credential} from '../components/authentication/authentication.service';

export class MainController {
  public classAnimation: string;
  public creationDate: number;
  public toastr: any;
  public credential: Credential;
  
  private loginManager: LoginManager;
  private $location: ng.ILocationService;

  /* @ngInject */
  constructor (loginManager: LoginManager, $location: ng.ILocationService, $timeout: ng.ITimeoutService, toastr: any) {
    this.$location = $location;
    this.loginManager = loginManager;
    
    this.classAnimation = '';
    this.creationDate = 1447939159410;
    this.toastr = toastr;
    this.activate($timeout);
    
    this.loginManager.getPrincipal()
      .then((credential:Credential) => {
        this.credential = credential;
      },
      () =>{
        this.$location.path('/login');
      });
    
  }

  /** @ngInject */
  activate($timeout: ng.ITimeoutService) {

    var self = this;

    $timeout(function() {
      self.classAnimation = 'rubberBand';
    }, 4000);
  }

  showToastr() {
    this.toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
    this.classAnimation = '';
  }
  
  logoff(){
    this.loginManager.doLogout();
    this.$location.path("/login");
  }
}
