/** @ngInject */
export function routerConfig($routeProvider: ng.route.IRouteProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    })
    .when('/login', {
      templateUrl: 'app/login/login.html',
      controller: 'LoginController',
      controllerAs: 'login'
    })
    .otherwise({
      redirectTo: '/'
    });
}
