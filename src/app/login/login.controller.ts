import { LoginManager } from "../components/login-manager/login-manager.service";
import { Credential } from "../components/authentication/authentication.service";


export class LoginController {

	public credential: Credential;

	private loginManager: LoginManager;
	private $location: ng.ILocationService;
	
	/** @ngInject */
	constructor(loginManager: LoginManager, $location: ng.ILocationService) {
		this.loginManager = loginManager;
		this.credential = new Credential();
		this.$location = $location;
	}

	doLogin() {
		this.loginManager.doLogin(this.credential)
			.then(() => {
				this.$location.path('/');
			});
	}

}